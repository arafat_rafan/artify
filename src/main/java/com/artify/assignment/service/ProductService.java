package com.artify.assignment.service;

import com.artify.assignment.model.Product;
import com.artify.assignment.model.Reservation;
import com.artify.assignment.repository.ProductRepository;
import com.artify.assignment.repository.ReserveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ReserveRepository reserveRepository;

    public String createProduct(Product product){
        productRepository.save(product);
        return product.getName() + " stored!";
    }
    public String buyProduct(Product product){
        Product existingProduct = findProduct(product.getId());
        if(existingProduct != null){
            System.out.println("product.getId() " + product.getId());
            int remainingStock = existingProduct.getStock() - (product.getStock() + totalReservedStock(product.getId()));
            if(remainingStock > 0){
                existingProduct.setStock(remainingStock);
                productRepository.save(existingProduct);
                return product.getName() + " has bought "+ product.getStock() + " item(s)!";
            }else {
                return "At this momment, " + product.getName() + " does not have enough stock to buy!";
            }
        }
        return product.getName() + " does not exist!";
    }
    public String refillProduct(Product product){
        Product existingProduct = findProduct(product.getId());
        if(existingProduct != null){
            existingProduct.setStock(existingProduct.getStock() + product.getStock());
            productRepository.save(existingProduct);
            return product.getName() + " has refilled!";
        }
        return product.getName() + " does not exist!";
    }
    public int findStock(String id){
        Product product = findProduct(id);
        if(product != null){
            return product.getStock();
        }
        return 0;
    }
    public List<Product> findAllProduct(){
        return productRepository.findAll();
    }
    public String reserveProduct(Reservation reservation, int time){
        Product product = findProduct(reservation.getProductID());
        if(product != null){
            changeReserveStatus(product, true);
            reservation.setValidity(System.currentTimeMillis() + time);
            reserveRepository.save(reservation);
        }
        return product.getName() + " reserved!";
    }
    public int totalReservedStock(String productID){
        List<Reservation> reservations = reserveRepository.findAllByProductID(productID);
        int reservedStock = 0;
        for (Reservation reservation: reservations
             ) {
            long remainingTime = reservation.getValidity() - System.currentTimeMillis();
            if(remainingTime > 0){
                reservedStock += reservation.getStock();
            }else{
                reserveRepository.deleteById(reservation.getId());
            }
        }
        if(reservedStock == 0 ){
            changeReserveStatus(findProduct(productID), false);
        }
        return reservedStock;
    }
    public void changeReserveStatus(Product product, boolean status){
        product.setReserveStock(status);
        productRepository.save(product);
    }
    public Product findProduct(String id){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if(!optionalProduct.isEmpty()){
            return optionalProduct.get();
        }
        return null;
    }
}
