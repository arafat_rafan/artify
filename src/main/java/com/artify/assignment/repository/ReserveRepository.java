package com.artify.assignment.repository;

import com.artify.assignment.model.Reservation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReserveRepository extends MongoRepository<Reservation, String> {
    List<Reservation> findAllByProductID(String productID);
}
