package com.artify.assignment.controller;

import com.artify.assignment.model.Product;
import com.artify.assignment.model.Reservation;
import com.artify.assignment.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/artify/product/")
public class ProductStockController {

    @Autowired
    ProductService productService;
    @Autowired
    ProductStockController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    List<Product> getProducts() {
        return productService.findAllProduct();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    Product getProduct(@PathVariable("id") String id) {
        return productService.findProduct(id);
    }
    @RequestMapping(value = "stock/{id}", method = RequestMethod.GET)
    int getStock(@PathVariable("id") String id) {
        return productService.findStock(id);
    }
    @RequestMapping(value = "buy/", method = RequestMethod.DELETE, headers = "Accept=application/json")
    String buyProduct(@RequestBody Product product) {
        return productService.buyProduct(product);
    }
    @RequestMapping(value = "refill/", method = RequestMethod.PUT, headers = "Accept=application/json")
    String refillProduct(@RequestBody Product product) {
        return productService.refillProduct(product);
    }
    @RequestMapping(value = "create/", method = RequestMethod.POST, headers = "Accept=application/json")
    String createProduct(@RequestBody Product product) {
        return productService.createProduct(product);
    }
    @RequestMapping(value = "reserve/{time}", method = RequestMethod.POST, headers = "Accept=application/json")
    String reserveProduct(@PathVariable("time") int time, @RequestBody Reservation reservation) {
        return productService.reserveProduct(reservation, time);
    }
}